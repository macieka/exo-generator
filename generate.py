import argparse

import exogenerator

parser = argparse.ArgumentParser(description='Allocation report generation tool')
parser.add_argument(dest='year_month', type=str, help='Month of reported hours, e.g. 2018/2', metavar='year/month')

# TODO: arguments parsing with error reporting
args = parser.parse_args()

session = exogenerator.Core(args.year_month)

session.generate_alloc_report()

# session.generate_psu_report()
