import xlsxwriter


class XlsPsu(object):

    def __init__(self, config):
        self.config = config
        self.user_name = config['user_name']
        self.user_surname = config['user_surname']
        self.bu = config['bu']
        self.file_name = self.user_name + '_' + self.user_surname + '_psu.xlsx'

    def generate_xls(self, project_time, year_month):
        ym = year_month.split('/')
        month = "{:02d}".format(int(ym[1])) + '/' + ym[0]

        workbook = xlsxwriter.Workbook(self.file_name)
        worksheet = self.create_headers(workbook)
        border = workbook.add_format({'border': 1})

        current_row = 1
        worksheet.conditional_format('A10:L25', {'type': 'no_blanks', 'format': border})

        if self.config['healthcare_include'] == 1:
            worksheet.write('A11', 'Program Lojalnościowy', border)
            worksheet.write('C11', self.config['bu'], border)
            worksheet.write('D11', self.config['healthcare_code'], border)
            worksheet.write('E11', '', border)
            worksheet.write('F11', self.config['healthcare_value'], border)
            worksheet.write('K11', self.config['healthcare_value'], border)

        for project_number, hours in project_time.items():
            worksheet.write(current_row, 0, self.user_surname)
            worksheet.write(current_row, 1, self.user_name)
            worksheet.write(current_row, 2, self.bu)
            worksheet.write(current_row, 3, project_number)
            worksheet.write(current_row, 4, hours)
            worksheet.write(current_row, 5, month)

            current_row += 1

        workbook.close()

    # @staticmethod
    def create_headers(self, workbook):
        worksheet = workbook.add_worksheet()
        grey = workbook.add_format()
        grey.set_bg_color('grey')

        border_grey = workbook.add_format({'border': 1, 'bg_color': 'grey'})

        worksheet.write('D3', 'Protokół Świadczenia Usług')
        worksheet.write('K1', 'Załącznik 3')
        worksheet.write('A5', 'I. Informacje podstawowe')
        worksheet.write('A6', 'Sporządzony przez', grey)
        worksheet.write('A7', 'Okres rozliczenia', grey)

        worksheet.write('B6', self.user_name + ' ' + self.user_surname)
        worksheet.write('B7', self.user_name + ' ' + self.user_surname)

        worksheet.write('A9', 'II. Wykaz zadań i czynności wykonanych')

        worksheet.merge_range('A10:B10', 'Zadanie', border_grey)
        worksheet.write('C10', 'MPK*', border_grey)
        worksheet.write('D10', 'Projekt*', border_grey)
        worksheet.write('E10', '"Liczba\n dni/godz.\n/ data**', border_grey)
        worksheet.write('F10', 'Wynagrodzenie razem, w tym:', border_grey)
        worksheet.write('G10', 'stałe', border_grey)
        worksheet.write('H10', 'dodatkowe', border_grey)
        worksheet.write('I10', 'poza RP', border_grey)
        worksheet.write('J10', 'zwrot kosztów', border_grey)
        worksheet.write('K10', 'inne', border_grey)
        worksheet.write('L10', 'Uwagi', border_grey)

        return worksheet
