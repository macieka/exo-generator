import calendar
import configparser
import os

from exogenerator.xlsPsu import XlsPsu
from .redmine import Redmine
from .xls import Xls


class Core(object):
    ZZ_PROJECT_ID = 74

    def __init__(self, year_month):
        config = configparser.ConfigParser()
        config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

        self.api_key = config['REDMINE']['api_key']
        self.user_id = config['REDMINE']['user_id']
        self.redmine_host = config['REDMINE']['host']

        self.config_xls = config['XLS']
        self.red = Redmine(self.api_key, self.redmine_host)

        self.year_month = year_month

        self.date_range = self.parse_date_argument(year_month)

        # TODO: custom time range
        self.time_entries = self.red.get_time_entries(self.user_id, self.date_range)

    def generate_alloc_report(self):
        project_time = self.get_time_in_projects(self.time_entries)

        xls = Xls(self.config_xls)
        xls.generate_xls(project_time, self.year_month)

    def get_time_in_projects(self, time_entries):
        projects = self.red.get_projects()
        project_time = {}

        for time_entry in time_entries['time_entries']:
            project_id = time_entry['project']['id']
            project_code = self.get_project_code(projects[project_id])

            if project_code is '' and project_id != self.ZZ_PROJECT_ID:
                raise ValueError("No project code for project_id {project_id}".format(project_id=project_id))

            if project_id == self.ZZ_PROJECT_ID:
                issue = self.red.get_issue(time_entry['issue']['id'])
                project_code = issue['issues'][0]['description']

            if project_code in project_time:
                project_time[project_code] += time_entry['hours']
            else:
                project_time[project_code] = time_entry['hours']

        return project_time

    @staticmethod
    def get_project_code(project):
        for field in project['custom_fields']:
            if field['id'] == 7:
                return field['value']
        return None

    @staticmethod
    def parse_date_argument(year_month):
        dates = year_month.split('/')
        month = "{:02d}".format(int(dates[1]))
        month_one_digit = int("{:2d}".format(int(dates[1])))
        year = dates[0]
        last_day = calendar.monthrange(int(year), month_one_digit)[1]
        return '><' + year + '-' + month + '-01' + '|' + year + '-' + month + '-' + str(last_day)

    def generate_psu_report(self):
        project_time = self.get_time_in_projects(self.time_entries)
        xls = XlsPsu(self.config_xls)
        xls.generate_xls(project_time, self.year_month)
