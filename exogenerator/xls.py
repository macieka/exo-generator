import xlsxwriter


class Xls(object):

    def __init__(self, config):
        self.user_name = config['user_name']
        self.user_surname = config['user_surname']
        self.bu = config['bu']
        self.file_name = self.user_name + '_' + self.user_surname + '_alokacja.xlsx'

    def generate_xls(self, project_time, year_month):
        ym = year_month.split('/')
        month = "{:02d}".format(int(ym[1])) + '_' + ym[0]

        workbook = xlsxwriter.Workbook(self.file_name)
        worksheet = self.create_headers(workbook)

        current_row = 1
        for project_number, hours in project_time.items():
            worksheet.write(current_row, 0, self.user_surname)
            worksheet.write(current_row, 1, self.user_name)
            worksheet.write(current_row, 2, self.bu)
            worksheet.write(current_row, 3, project_number)
            worksheet.write(current_row, 4, hours)
            worksheet.write(current_row, 5, month)

            current_row += 1
        workbook.close()

    @staticmethod
    def create_headers(workbook):
        worksheet = workbook.add_worksheet()
        yellow = workbook.add_format()
        yellow.set_bg_color('yellow')
        worksheet.set_column('A:A', 15)
        worksheet.set_column('B:B', 15)
        worksheet.set_column('D:D', 15)
        worksheet.set_column('E:E', 20)
        worksheet.write('A1', 'NAZWISKO', yellow)
        worksheet.write('B1', 'IMIĘ', yellow)
        worksheet.write('C1', 'BU/Dział', yellow)
        worksheet.write('D1', 'NR PROJEKTU', yellow)
        worksheet.write('E1', 'LICZBA GODZIN', yellow)
        worksheet.write('F1', 'MIESIĄC', yellow)

        return worksheet
