import sys

import requests


class Redmine(object):
    endpoints = {'projects': '/projects.json', 'time': '/time_entries.json', 'issues': '/issues.json'}

    def __init__(self, api_key, redmine_host):
        self.redmine_host = redmine_host
        self.headers = {'X-Redmine-API-Key': api_key}

    def get_projects(self):
        address = self.redmine_host + self.endpoints['projects']
        params = {'limit': 100}

        try:
            r = requests.get(address, headers=self.headers, params=params)
            response = r.json()
            projects = {}

            for project in response['projects']:
                projects[project['id']] = project

            return projects
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)

    def get_time_entries(self, user_id, time):
        address = self.redmine_host + self.endpoints['time']
        params = {'user_id': user_id, 'spent_on': time, 'limit': 100}

        try:
            r = requests.get(address, headers=self.headers, params=params)
            return r.json()
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)

    def get_issue(self, issue_id):
        address = self.redmine_host + self.endpoints['issues']
        params = {'issue_id': issue_id}

        try:
            r = requests.get(address, headers=self.headers, params=params)
            return r.json()
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)