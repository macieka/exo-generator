from setuptools import setup

setup(name='exogenerator',
      version='0.1',
      description='Exorigo reports creation utility',
      author='Maciej Andrejczuk',
      author_email='maciej.andrejczuk@exorigo-upos.pl',
      license='MIT',
      packages=['exogenerator'],
      install_requires=[
          'requests',
          # 'calendar',
          'configparser==3.5.',
          'xlsxwriter'
      ],
      scripts=['bin/exogenerator'],
      zip_safe=False)
